<!-- Top Bar Start -->
<div class="topbar">
    <div class="topbar-left	d-none d-lg-block">
      <div class="text-center">
        <a href="#" class="logo">
          <img src="{{ asset('logo.png') }}" height="40" alt="logo" style="margin-top: -24px;"> 
          <h5 style="margin-top:-25px;color:#fff;">Toko Sastramas</h5>
        </a>
      </div>
    </div>
    <nav class="navbar-custom">
      <ul class="list-inline float-right mb-0"> 
        <li class="list-inline-item dropdown notification-list">
          <a class="nav-link dropdown-toggle arrow-none waves-effect nav-user" data-toggle="dropdown" href="#" role="button" aria-haspopup="false" aria-expanded="false">
            <img src="{{ asset('logo.png') }}" alt="user" class="rounded-circle" width="30" height="30">
          </a>
          <div class="dropdown-menu dropdown-menu-right dropdown-menu-animated profile-dropdown"> 
            <a class="dropdown-item" href="#">
              <i class="fa fa-sign-out m-r-5 text-muted"></i> Logout </a>
          </div>
        </li>
      </ul>
      <ul class="list-inline menu-left mb-0">
        <li class="list-inline-item">
          <button type="button" class="button-menu-mobile open-left waves-effect">
            <i class="fa fa-tasks"></i>
          </button>
        </li>
      </ul>
      <div class="clearfix"></div>
    </nav>
  </div>
  <!-- Top Bar End -->
