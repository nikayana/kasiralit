<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>Dashboard</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="ThemeDesign" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="https://themesdesign.in/drixo/vertical-red/assets/images/favicon.ico">
    <!-- Plugins css -->
    <link href="https://themesdesign.in/drixo/vertical-red/assets/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
    <link href="https://themesdesign.in/drixo/vertical-red/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet">
    <link href="https://themesdesign.in/drixo/vertical-red/assets/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css" rel="stylesheet">

    <link href="https://themesdesign.in/drixo/vertical-red/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://themesdesign.in/drixo/vertical-red/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="https://themesdesign.in/drixo/vertical-red/assets/css/style.css" rel="stylesheet" type="text/css">

    <style>
      .navbar-custom{
        background-color: #0074a9;
      }
      .button-menu-mobile{
        background-color: #0074a9;
      }
      .bg-primary{
        background-color: #0074a9 !important;
      }
      .topbar .topbar-left{
        background-color: #0074a9 !important;
      }
    </style>
  </head>
  <body class="fixed-left">
    <!-- Loader -->
    <div id="preloader">
      <div id="status">
        <div class="spinner"></div>
      </div>
    </div>
    <!-- Begin page -->
    <div id="wrapper">
        <!-- ========== Left Sidebar Start ========== -->
        <div class="left side-menu">
            <button type="button" class="button-menu-mobile button-menu-mobile-topbar open-left waves-effect">
                <i class="fa fa-close"></i>
            </button>
            <div class="left-side-logo d-block d-lg-none">
            <div class="text-center">
                <a href="#" class="logo">
                  <img src="{{ asset('logo.png') }}" height="40" alt="logo" style="margin-top: -24px;">
                </a>
            </div>
            </div>
            <div class="sidebar-inner slimscrollleft">
            <div id="sidebar-menu">
                <ul>
                    <li class="menu-title">Menu</li>
                    <li>
                        <a href="/" class="waves-effect">
                        <i class="fa fa-user"></i>
                        <span>Dashboard <span class="badge badge-success badge-pill float-right"></span>
                        </span>
                        </a>
                    </li>
                    <li>
                        <a href="/laporan/barang" class="waves-effect">
                        <i class="fa fa-box"></i>
                        <span>Stok Barang <span class="badge badge-success badge-pill float-right"></span>
                        </span>
                        </a>
                    </li>
                    <li>
                        <a href="/laporan/penjualan" class="waves-effect">
                        <i class="fa fa-tag"></i>
                        <span>Penjualan <span class="badge badge-success badge-pill float-right"></span>
                        </span>
                        </a>
                    </li>
                    <li>
                        <a href="/laporan/pembelian" class="waves-effect">
                        <i class="fa fa-tag"></i>
                        <span>Pembelian <span class="badge badge-success badge-pill float-right"></span>
                        </span>
                        </a>
                    </li>
                    <li>
                      <a href="/laporan/pendapatan" class="waves-effect">
                      <i class="fa fa-beer"></i>
                      <span>Pendapatan <span class="badge badge-success badge-pill float-right"></span>
                      </span>
                      </a>
                    </li>
                    <li>
                      <a href="/laporan/pengeluaran" class="waves-effect">
                      <i class="fa fa-book"></i>
                      <span>Pengeluaran <span class="badge badge-success badge-pill float-right"></span>
                      </span>
                      </a>
                    </li>  
                    <li>
                      <a href="/laporan/supplier" class="waves-effect">
                      <i class="fa fa-book"></i>
                      <span>Supplier <span class="badge badge-success badge-pill float-right"></span>
                      </span>
                      </a>
                    </li>  
                </ul>
            </div>
            <div class="clearfix"></div>
            </div>
            <!-- end sidebarinner -->
        </div>
        <!-- Left Sidebar End -->

        @yield('content')
    </div>
    <!-- END wrapper -->
    <!-- jQuery  -->
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.min.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/modernizr.min.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/detect.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/fastclick.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.slimscroll.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.blockUI.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/waves.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.nicescroll.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.scrollTo.min.js"></script>
    <script src="https://kit.fontawesome.com/88481955d1.js" crossorigin="anonymous"></script>

    <!-- Plugins js -->
    <script src="https://themesdesign.in/drixo/vertical-red/assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js"></script>

    <!-- App js --> 
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/app.js"></script>

    @yield('js')
  </body>
</html>