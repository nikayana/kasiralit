@extends('layouts.app')

@section('content')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
      @include('layouts.topbar')

      <div class="page-content-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="float-right page-breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="/">Home</a>
                  </li> 
                  <li class="breadcrumb-item active">Dashboard</li>
                </ol>
              </div>
              <h5 class="page-title">Dashboard</h5>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="card m-b-30">
                <div class="card-body">
                    <h4 class="mt-0 header-title">Pencarian</h4> 
                    <form action="/" method="GET">  
                        <div class="form-group">
                            <label>Pilih Tanggal</label>
                            <div>
                            <div class="input-daterange input-group" id="date-range">
                                <input type="text" class="form-control" name="start" placeholder="Start Date" value="{{ date('m/d/Y', strtotime($response['tgl_awal'])) }}">
                                <input type="text" class="form-control" name="end" placeholder="End Date" value="{{ date('m/d/Y', strtotime($response['tgl_akhir'])) }}">
                            </div>
                            </div>
                        </div> 
                        <div class="form-group">
                            <div class="button-items">
                                <button type="submit" class="btn btn-primary waves-effect waves-light">Cari Data</button> 
                            </div>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="card m-b-30">
                <div class="card-body">
                  <h4 class="mt-0 header-title">Penjualan {{ date('d M Y', strtotime($response['tgl_awal'])) }} s/d {{ date('d M Y', strtotime($response['tgl_akhir'])) }} <a href="/laporan/penjualan"><button class="btn btn-primary">Lihat Semua</button></a></h4>
                  <div class="row">
                    <div class="col-xl-4 col-md-6">
                      <div class="card mini-stat m-b-30">
                        <div class="p-3 bg-primary text-white">
                          <div class="mini-stat-icon">
                            <i class="fa fa-tag float-right mb-0"></i>
                          </div>
                          <h6 class="text-uppercase mb-0">Penjualan</h6>
                        </div>
                        <div class="card-body">
                          <div class="border-bottom pb-4">
                            <span class="badge badge-danger"></span>
                            <h6 class="ml-2 text-muted">Rp.{{ number_format($response['harga_jual']) }}</h6>
                          </div> 
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                      <div class="card mini-stat m-b-30">
                        <div class="p-3 bg-primary text-white">
                          <div class="mini-stat-icon">
                            <i class="fa fa-tag float-right mb-0"></i>
                          </div>
                          <h6 class="text-uppercase mb-0">Harga Pokok</h6>
                        </div>
                        <div class="card-body">
                          <div class="border-bottom pb-4">
                            <span class="badge badge-danger"></span>
                            <h6 class="ml-2 text-muted">Rp.{{ number_format($response['harga_pokok']) }}</h6>
                          </div> 
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                      <div class="card mini-stat m-b-30">
                        <div class="p-3 bg-primary text-white">
                          <div class="mini-stat-icon">
                            <i class="fa fa-tag float-right mb-0"></i>
                          </div>
                          <h6 class="text-uppercase mb-0">Laba/Rugi</h6>
                        </div>
                        <div class="card-body">
                          <div class="border-bottom pb-4">
                            <span class="badge badge-danger"></span>
                            <h6 class="ml-2 text-muted">Rp.{{ number_format($response['harga_jual'] - $response['harga_pokok']) }}</h6>
                          </div> 
                        </div>
                      </div>
                    </div>
                  </div>
 
                  <div>
                    {!! $response['grafik_penjualan']->container() !!}
                  </div>
                </div>
              </div> 
            </div> 
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="card m-b-30">
                <div class="card-body">
                  <h4 class="mt-0 header-title">Pembelian {{ date('d M Y', strtotime($response['tgl_awal'])) }} s/d {{ date('d M Y', strtotime($response['tgl_akhir'])) }} <a href="/laporan/pembelian"><button class="btn btn-primary">Lihat Semua</button></a></h4>
                  <div class="row">
                    <div class="col-xl-3 col-md-6">
                      <div class="card mini-stat m-b-30">
                        <div class="p-3 bg-primary text-white">
                          <div class="mini-stat-icon">
                            <i class="fa fa-tag float-right mb-0"></i>
                          </div>
                          <h6 class="text-uppercase mb-0">TOTAL PEMBELIAN</h6>
                        </div>
                        <div class="card-body">
                          <div class="border-bottom pb-4">
                            <span class="badge badge-danger"></span>
                            <h6 class="ml-2 text-muted">Rp.{{ number_format($response['total_pembelian']) }}</h6>
                          </div> 
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <div class="card mini-stat m-b-30">
                        <div class="p-3 bg-primary text-white">
                          <div class="mini-stat-icon">
                            <i class="fa fa-tag float-right mb-0"></i>
                          </div>
                          <h6 class="text-uppercase mb-0">CASH</h6>
                          <p>Pembelian secara cash</p>
                        </div>
                        <div class="card-body">
                          <div class="border-bottom pb-4">
                            <span class="badge badge-danger"></span>
                            <h6 class="ml-2 text-muted">Rp.{{ number_format($response['sub_pembelian_cash']) }}</h6>
                          </div> 
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <div class="card mini-stat m-b-30">
                        <div class="p-3 bg-primary text-white">
                          <div class="mini-stat-icon">
                            <i class="fa fa-tag float-right mb-0"></i>
                          </div>
                          <h6 class="text-uppercase mb-0">KREDIT</h6>
                          <p>Pembelian secara kredit</p>
                        </div>
                        <div class="card-body">
                          <div class="border-bottom pb-4">
                            <span class="badge badge-danger"></span>
                            <h6 class="ml-2 text-muted">Rp.{{ number_format($response['sub_pembelian_kredit']) }}</h6>
                          </div> 
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-3 col-md-6">
                      <div class="card mini-stat m-b-30">
                        <div class="p-3 bg-primary text-white">
                          <div class="mini-stat-icon">
                            <i class="fa fa-tag float-right mb-0"></i>
                          </div>
                          <h6 class="text-uppercase mb-0">KREDIT BELUM LUNAS</h6>
                          <p>Total pembelian secara kredit belum lunas</p>
                        </div>
                        <div class="card-body">
                          <div class="border-bottom pb-4">
                            <span class="badge badge-danger"></span>
                            <h6 class="ml-2 text-muted">Rp.{{ number_format($response['sub_pembelian_kredit_belum_lunas']) }}</h6>
                          </div> 
                        </div>
                      </div>
                    </div>
                  </div>
 
                  <div>
                    {{-- {!! $response['grafik_penjualan']->container() !!} --}}
                  </div>
                </div>
              </div> 
            </div> 
          </div>

          <div class="row">
            <div class="col-sm-12">
              <div class="card m-b-30">
                <div class="card-body">
                  <h4 class="mt-0 header-title">Pendapatan & Pengeluaran {{ date('d M Y', strtotime($response['tgl_awal'])) }} s/d {{ date('d M Y', strtotime($response['tgl_akhir'])) }} </h4>
                  <div class="row">
                    <div class="col-xl-4 col-md-6">
                      <div class="card mini-stat m-b-30">
                        <a href="/laporan/pendapatan?start={{ $response['tgl_awal'] }}&end={{ $response['tgl_akhir'] }}">
                        <div class="p-3 bg-primary text-white">
                          <div class="mini-stat-icon">
                            <i class="fa fa-tag float-right mb-0"></i>
                          </div>
                          <h6 class="text-uppercase mb-0">Pendapatan</h6>
                        </div>
                        </a>
                        <div class="card-body">
                          <div class="border-bottom pb-4">
                            <span class="badge badge-danger"></span>
                            <h6 class="ml-2 text-muted">Rp.{{ number_format($response['pendapatan']) }}</h6>
                          </div> 
                        </div>
                      </div>
                    </div>
                    <div class="col-xl-4 col-md-6">
                      <div class="card mini-stat m-b-30">
                        <a href="/laporan/pengeluaran?start={{ $response['tgl_awal'] }}&end={{ $response['tgl_akhir'] }}">
                        <div class="p-3 bg-primary text-white">
                          <div class="mini-stat-icon">
                            <i class="fa fa-tag float-right mb-0"></i>
                          </div>
                          <h6 class="text-uppercase mb-0">Pengeluaran</h6>
                        </div>
                        </a>
                        <div class="card-body">
                          <div class="border-bottom pb-4">
                            <span class="badge badge-danger"></span>
                            <h6 class="ml-2 text-muted">Rp.{{ number_format($response['pengeluaran']) }}</h6>
                          </div> 
                        </div>
                      </div>
                    </div> 
                  </div> 
                </div>
              </div> 
            </div> 
          </div>

           

           

        </div>
        <!-- container fluid -->
      </div>
      <!-- Page content Wrapper -->
    </div>
    <!-- content -->
    <footer class="footer">© 2023 <b></b> 
    </footer>
</div>
<!-- End Right content here -->
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/highcharts/6.0.6/highcharts.js" charset="utf-8"></script>
<script>
  jQuery("#date-range").datepicker({
      toggleActive: !0,
      autoclose: !0,
  }); 
</script>

{!! $response['grafik_penjualan']->script() !!}
{!! $response['grafik_pembelian']->script() !!}

@endsection