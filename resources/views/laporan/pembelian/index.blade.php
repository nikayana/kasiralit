@extends('layouts.app')

@section('content')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
      @include('layouts.topbar')

      <div class="page-content-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="float-right page-breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="#">Home</a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="#">Laporan</a>
                  </li>
                  <li class="breadcrumb-item active">Pembelian</li>
                </ol>
              </div>
              <h5 class="page-title">Laporan Pembelian</h5>
            </div>

            <div class="col-sm-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Pencarian</h4> 
                        <form action="/laporan/pembelian" method="GET">  
                            <div class="form-group">
                                <label>Pilih Tanggal</label>
                                <div>
                                <div class="input-daterange input-group" id="date-range">
                                    <input type="text" class="form-control" name="start" placeholder="Start Date" value="{{ date('m/d/Y', strtotime($response['tgl_awal'])) }}">
                                    <input type="text" class="form-control" name="end" placeholder="End Date" value="{{ date('m/d/Y', strtotime($response['tgl_akhir'])) }}">
                                </div>
                                </div>
                            </div> 

                            <div class="form-group">
                                <div class="button-items">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Cari Data</button> 
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="card-body">
                      <h4 class="mt-0 header-title">Pembelian dari {{ date('d M Y', strtotime($response['tgl_awal'])) }} s/d  {{ date('d M Y', strtotime($response['tgl_akhir'])) }}</h4> 
                      
                      <div class="row">
                        <div class="col-xl-3 col-md-6">
                          <div class="card mini-stat m-b-30">
                            <div class="p-3 bg-primary text-white">
                              <div class="mini-stat-icon">
                                <i class="fa fa-credit-card float-right mb-0"></i>
                              </div>
                              <h6 class="text-uppercase mb-0">Total Pembelian</h6>
                            </div>
                            <div class="card-body">
                              <div class="border-bottom pb-2"> 
                                <span class="ml-2 text-muted"><h5>Rp.  {{ number_format($response['total']) }}</h5></span>
                              </div>
                              <div class="mt-4 text-muted"> 
                                <h5 class="m-0"><i class="fa fa-file text-success ml-2"></i> {{ $response['total_row'] }}
                                </h5>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="table-responsive">
                        <table class="table table-striped mb-0">
                          <thead>
                            <tr>
                              <th>#Nota</th>
                              <th>Tanggal/Jam</th>
                              <th>Suplier</th>
                              <th>Item Barang</th>
                              <th>Total</th>
                              <th>Status</th> 
                              <th>Ket</th> 
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($response['data'] as $item)
                                <tr>
                                    <th scope="row"><a href="/laporan/pembelian/{{ $item->No_Faktur_beli }}">{{ $item->No_Faktur_beli }}</a></th>
                                    <td>{{ date('d M Y', strtotime($item->Tanggal)) }} {{ $item->jam }}</td>
                                    <td>{{ $item->kode_supplier.' '.$item->nama }}</td>
                                    <td>{{ $item->jmlh_Item }}</td> 
                                    <td>Rp.{{ number_format($item->Sub_total) }}</td>
                                    <td>{{ $item->Status }}</td>
                                    <td>{{ $item->Status_Bayar }}</td> 
                                </tr> 
                            @endforeach 
                          </tbody>
                        </table>
                        <br>
                        <div>
                            {{ $response['data']->withQueryString()->links() }}
                        </div>
                      </div>
                    </div>
                </div>
            </div>

          </div>
        </div>
        <!-- container fluid -->
      </div>
      <!-- Page content Wrapper -->
    </div>
    <!-- content -->
    <footer class="footer">© 2023 <b></b> 
    </footer>
</div>
<!-- End Right content here -->
@endsection

@section('js')
<script>
    jQuery("#date-range").datepicker({
        toggleActive: !0,
        autoclose: !0,
    })
</script>
@endsection