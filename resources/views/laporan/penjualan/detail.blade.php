@extends('layouts.app')

@section('content')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
      @include('layouts.topbar')

      <div class="page-content-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="float-right page-breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="#">Home</a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="#">Laporan</a>
                  </li>
                  <li class="breadcrumb-item active">Penjualan</li>
                </ol>
              </div>
              <h5 class="page-title">Detail Penjualan</h5>
            </div>

            <div class="col-sm-12">
                <div class="card m-b-30"> 
                    <div class="card-body">
                      <h4 class="mt-0 header-title">No Faktur dari {{ $response['data']->no_faktur_jual }}</h4> 
                      
                      <div class="row">
                        <div class="col-xl-3 col-md-6">
                          <div class="card mini-stat m-b-30">
                            <div class="p-3 bg-primary text-white">
                              <div class="mini-stat-icon">
                                <i class="fa fa-credit-card float-right mb-0"></i>
                              </div>
                              <h6 class="text-uppercase mb-0">Total Penjualan</h6>
                            </div>
                            <div class="card-body">
                              <div class="border-bottom pb-2"> 
                                <span class="ml-2 text-muted"><h3>Rp.  {{ number_format($response['data']->total) }}</h3></span>
                              </div>
                              <div class="mt-4 text-muted"> 
                                
                                </h5>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="table-responsive">
                        <table class="table table-striped mb-0">
                          <thead>
                            <tr>
                              <th>#Nota</th>
                              <th>Kode</th>
                              <th>Barang</th> 
                              <th>Harga Pokok</th>
                              <th>Harga Jual</th>
                              <th>Qty</th>
                              <th>Satuan</th>
                              <th>Diskon</th>
                              <th>Sub Total</th>
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($response['detail'] as $item)
                                <tr>
                                    <th scope="row"><a href="/laporan/penjualan/{{ $item->no_faktur_jual }}">{{ $item->no_faktur_jual }}</a></th> 
                                    <td>{{ $item->kode }}</td>
                                    <td>{{ $item->uraian }}</td>
                                    <td>Rp.{{ number_format($item->harga_pokok) }}</td>
                                    <td>Rp.{{ number_format($item->harga_jual) }}</td>
                                    <td>{{ number_format($item->qty) }}</td>
                                    <td>{{ $item->satuan }}</td>
                                    <td>Rp.{{ number_format($item->diskon) }}</td>
                                    <td>Rp.{{ number_format($item->sub_total) }}</td>
                                </tr> 
                            @endforeach 
                          </tbody>
                        </table> 
                      </div>
                    </div>
                </div>
            </div>

          </div>
        </div>
        <!-- container fluid -->
      </div>
      <!-- Page content Wrapper -->
    </div>
    <!-- content -->
    <footer class="footer">© 2023 <b></b> 
    </footer>
</div>
<!-- End Right content here -->
@endsection

@section('js')
<script>
    jQuery("#date-range").datepicker({
        toggleActive: !0,
        autoclose: !0,
    })
</script>
@endsection