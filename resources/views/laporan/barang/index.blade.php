@extends('layouts.app')

@section('content')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
      @include('layouts.topbar')

      <div class="page-content-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="float-right page-breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="#">Home</a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="#">Laporan</a>
                  </li>
                  <li class="breadcrumb-item active">Barang</li>
                </ol>
              </div>
              <h5 class="page-title">Laporan Barang</h5>
            </div>

            <div class="col-sm-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Pencarian</h4> 
                        <form action="/laporan/barang" method="GET">  
                            {{-- <div class="form-group">
                                <label>Pilih Tanggal Exp</label>
                                <div>
                                <div class="input-daterange input-group" id="date-range">
                                    <input type="text" class="form-control" name="start" placeholder="Start Date" value="{{ (isset($response['request']->start))? date('m/d/Y', strtotime($response['request']->start)) : '' }}">
                                    <input type="text" class="form-control" name="end" placeholder="End Date" value="{{ (isset($response['request']->end))? date('m/d/Y', strtotime($response['request']->end)) : '' }}">
                                </div>
                                </div>
                            </div> --}} 
                            <div class="form-group">
                              <label>Kode / Nama Barang</label>
                              <div>
                                <input type="text" class="form-control" name="Nama_brg" placeholder="Nama Barang" value="{{ (isset($response['request']['Nama_brg']))? $response['request']['Nama_brg'] : '' }}">
                              </div> 
                            </div>
                            
                            <div class="form-group">
                              <label>Jenis Barang</label>
                              <div>
                                <input type="text" class="form-control" name="Jenis_brg" placeholder="Jenis Barang" value="{{ (isset($response['request']['Jenis_brg']))? $response['request']['Jenis_brg'] : '' }}">
                              </div> 
                            </div>

                            <div class="form-group">
                              <label>Kondisi Stok (<=) Kurang Sama Dari Qty</label>
                              <div>
                                <div class="kondisi_barang input-group"> 
                                    <input type="number" class="form-control" name="qty" placeholder="Qty Barang" value="{{ (isset($response['request']['qty']))? $response['request']['qty'] : '' }}">
                                </div>
                                </div> 
                            </div>

                            <div class="form-group">
                                <div class="button-items">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Cari Data</button> 
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="card-body">
                      <h4 class="mt-0 header-title">Data Barang {{ $response['data']->total() }} Item</h4>  
                       

                      <div class="table-responsive">
                        <table class="table table-striped mb-0">
                          <thead>
                            <tr>
                              <th>#Kode</th>
                              <th>Nama</th>
                              <th>Jenis</th>
                              <th>Satuan</th>
                              <th>Harga Beli</th> 
                              <th>Harga Pokok</th> 
                              <th>Harga Grosir</th> 
                              <th>Harga Eceran</th>  
                              <th>Stok Akhir</th> 
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($response['data'] as $item)
                                <tr>
                                    <th scope="row">{{ $item->Kode_brg }}</th> 
                                    <td>{{ $item->Nama_brg }}</td> 
                                    <td>{{ $item->Jenis_brg }}</td>
                                    <td>{{ $item->Satuan }}</td>
                                    <td>Rp.{{ number_format($item->Hrg_beli )}}</td>
                                    <td>Rp.{{ number_format($item->Hrg_pokok )}}</td>
                                    <td>Rp.{{ number_format($item->Hrg_Grosir )}}</td>
                                    <td>Rp.{{ number_format($item->Hrg_Eceran )}}</td> 
                                    <td>{{ $item->Stok }}</td>
                                </tr> 
                            @endforeach 
                          </tbody>
                        </table>
                        <br>
                        <div class="row">
                          <div class="col-12">
                            {{ $response['data']->withQueryString()->links() }}
                          </div> 
                        </div>
                      </div>
                    </div>
                </div>
            </div>

          </div>
        </div>
        <!-- container fluid -->
      </div>
      <!-- Page content Wrapper -->
    </div>
    <!-- content -->
    <footer class="footer">© 2023 <b></b> 
    </footer>
</div>
<!-- End Right content here -->
@endsection

@section('js')
<script>
    jQuery("#date-range").datepicker({
        toggleActive: !0,
        autoclose: !0,
    })
</script>
@endsection