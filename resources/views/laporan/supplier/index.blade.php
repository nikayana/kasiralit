@extends('layouts.app')

@section('content')
<!-- Start right Content here -->
<div class="content-page">
    <!-- Start content -->
    <div class="content">
      @include('layouts.topbar')

      <div class="page-content-wrapper">
        <div class="container-fluid">
          <div class="row">
            <div class="col-sm-12">
              <div class="float-right page-breadcrumb">
                <ol class="breadcrumb">
                  <li class="breadcrumb-item">
                    <a href="#">Home</a>
                  </li>
                  <li class="breadcrumb-item">
                    <a href="#">Laporan</a>
                  </li>
                  <li class="breadcrumb-item active">Supplier</li>
                </ol>
              </div>
              <h5 class="page-title">Laporan Supplier</h5>
            </div>

            <div class="col-sm-12">
                <div class="card m-b-30">
                    <div class="card-body">
                        <h4 class="mt-0 header-title">Pencarian</h4> 
                        <form action="/laporan/supplier" method="GET">  
                            <div class="form-group">
                              <label>Kode / Nama Supplier</label>
                              <div>
                                <input type="text" class="form-control" name="keyword" placeholder="Nama/Kode" value="{{ (isset($response['request']['keyword']))? $response['request']['keyword'] : '' }}">
                              </div> 
                            </div> 

                            <div class="form-group">
                                <div class="button-items">
                                    <button type="submit" class="btn btn-primary waves-effect waves-light">Cari Data</button> 
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="card-body">
                      <h4 class="mt-0 header-title">Data Supplier {{ $response['data']->total() }} </h4>  
                       

                      <div class="table-responsive">
                        <table class="table table-striped mb-0">
                          <thead>
                            <tr>
                              <th>#Kode</th>
                              <th>Nama</th>
                              <th>Alamat</th>
                              <th>Tlp</th>
                              <th>Sisa Hutang</th>  
                            </tr>
                          </thead>
                          <tbody>
                            @foreach ($response['data'] as $item)
                                <tr>
                                    <th scope="row">{{ $item->Kode_supplier }}</th> 
                                    <td>{{ $item->nama_supplier }}</td> 
                                    <td>{{ $item->Alamat }}</td>
                                    <td>{{ $item->telp }}</td>
                                    <td>Rp.{{ number_format($item->hutang )}}</td> 
                                </tr> 
                            @endforeach 
                          </tbody>
                        </table>
                        <br>
                        <div class="row">
                          <div class="col-12">
                            {{ $response['data']->withQueryString()->links() }}
                          </div> 
                        </div>
                      </div>
                    </div>
                </div>
            </div>

          </div>
        </div>
        <!-- container fluid -->
      </div>
      <!-- Page content Wrapper -->
    </div>
    <!-- content -->
    <footer class="footer">© 2023 <b></b> 
    </footer>
</div>
<!-- End Right content here -->
@endsection

@section('js')
<script>
    jQuery("#date-range").datepicker({
        toggleActive: !0,
        autoclose: !0,
    })
</script>
@endsection