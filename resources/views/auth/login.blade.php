<!DOCTYPE html>
<html lang="en">
  <head>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
    <title>Login</title>
    <meta content="Admin Dashboard" name="description">
    <meta content="ThemeDesign" name="author">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="https://themesdesign.in/drixo/vertical-red/assets/images/favicon.ico">
    <link href="https://themesdesign.in/drixo/vertical-red/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="https://themesdesign.in/drixo/vertical-red/assets/css/icons.css" rel="stylesheet" type="text/css">
    <link href="https://themesdesign.in/drixo/vertical-red/assets/css/style.css" rel="stylesheet" type="text/css">
  </head>
  <body class="fixed-left">
    <!-- Loader -->
    <div id="preloader">
      <div id="status">
        <div class="spinner"></div>
      </div>
    </div>
    <!-- Begin page -->
    <div class="accountbg">
      <div class="content-center">
        <div class="content-desc-center">
          <div class="container">
            <div class="row justify-content-center">
              <div class="col-lg-5 col-md-8">
                <div class="card">
                  <div class="card-body">
                    <h3 class="text-center mt-0 m-b-15">
                      <a href="https://themesdesign.in/drixo/vertical-red/assetsindex.html" class="logo logo-admin">
                        <img src="https://themesdesign.in/drixo/vertical-red/assets/images/logo-dark.png" height="30" alt="logo">
                      </a>
                    </h3>
                    <h4 class="text-muted text-center font-18">
                      <b>Sign In</b>
                    </h4>
                    <div class="p-2">
                        <form method="POST" action="{{ route('dologin') }}">
                            @csrf
                        <div class="form-group row">
                          <div class="col-12">
                            <input class="form-control  @error('username') is-invalid @enderror" type="text" name="username" value="{{ old('username') }}" required autocomplete="Username" autofocus>
                            
                            @error('username')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-12">
                            <input class="form-control @error('password') is-invalid @enderror" type="password" required autocomplete="current-password" name="password">
                            
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                          </div>
                        </div>
                        <div class="form-group row">
                          <div class="col-12">
                            <div class="custom-control custom-checkbox">
                              <input type="checkbox" class="custom-control-input" id="customCheck1">
                              <label class="custom-control-label" for="customCheck1">Remember me</label>
                            </div>
                          </div>
                        </div>
                        <div class="form-group text-center row m-t-20">
                          <div class="col-12">
                            <button class="btn btn-primary btn-block waves-effect waves-light" type="submit">Log In</button>
                          </div>
                        </div>
                        {{-- <div class="form-group m-t-10 mb-0 row">
                          <div class="col-sm-7 m-t-20">
                            <a href="https://themesdesign.in/drixo/vertical-red/assetspages-recoverpw.html" class="text-muted">
                              <i class="mdi mdi-lock"></i> Forgot your password? </a>
                          </div>
                          <div class="col-sm-5 m-t-20">
                            <a href="https://themesdesign.in/drixo/vertical-red/assetspages-register.html" class="text-muted">
                              <i class="mdi mdi-account-circle"></i> Create an account </a>
                          </div>
                        </div> --}}
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- end row -->
          </div>
        </div>
      </div>
    </div>
    <!-- jQuery  -->
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.min.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/bootstrap.bundle.min.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/modernizr.min.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/detect.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/fastclick.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.slimscroll.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.blockUI.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/waves.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.nicescroll.js"></script>
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/jquery.scrollTo.min.js"></script>
    <!-- App js -->
    <script src="https://themesdesign.in/drixo/vertical-red/assets/js/app.js"></script>
  </body>
</html>