<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('login', 'Auth\LoginController@index')->name('login');
Route::post('post-login', 'Auth\LoginController@postLogin')->name('dologin'); 

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/laporan/penjualan', 'PenjualanController@index')->name('penjualan');
Route::get('/laporan/penjualan/{no_faktur}', 'PenjualanController@show')->name('penjualan.detail');
Route::get('/laporan/pembelian', 'PembelianController@index')->name('pembelian');
Route::get('/laporan/pembelian/{no_faktur}', 'PembelianController@show')->name('pembelian.detail');
Route::get('/laporan/barang', 'BarangController@index')->name('barang');
Route::get('/laporan/pendapatan', 'PendapatanController@index')->name('pendapatan');
Route::get('/laporan/pengeluaran', 'PengeluaranController@index')->name('pengeluaran');
Route::get('/laporan/supplier', 'SupliyerController@index')->name('supplier');
