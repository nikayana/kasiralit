<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('penjualan', 'API\PenjualanController@store')->name('store.penjualan'); 
Route::post('penjualan_detail', 'API\PenjualanDetailController@store')->name('store.penjualan_detail'); 
Route::post('barang', 'API\BarangController@store')->name('store.barang'); 
Route::post('pendapatan', 'API\PendapatanController@store')->name('store.pendapatan'); 
Route::post('pengeluaran', 'API\PengeluaranController@store')->name('store.pengeluaran'); 
Route::post('pembelian', 'API\PembelianController@store')->name('store.pembelian'); 
Route::post('pembelian_detail', 'API\PembelianDetailController@store')->name('store.pembelian_detail'); 
Route::post('supliyer', 'API\SupliyerController@store')->name('store.supliyer'); 

