<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('job:penjualan')->everyMinute();
        $schedule->command('job:penjualan_detail')->everyMinute();

        $schedule->command('job:pembelian')->everyMinute();
        $schedule->command('job:pembelian_detail')->everyMinute();

        $schedule->command('job:barang')->everyMinute();
        $schedule->command('job:pendapatan')->everyMinute();
        $schedule->command('job:pengeluaran')->everyMinute();
        $schedule->command('job:supliyer')->everyMinute();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
