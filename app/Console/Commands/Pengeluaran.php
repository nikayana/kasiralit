<?php

namespace App\Console\Commands;

use Illuminate\Console\Command; 

use GuzzleHttp\Client;
use App\Models\Pengeluaran as ModelPengeluaran; 

class Pengeluaran extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:pengeluaran';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Job pengeluaran';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = config('API_URL', 'https://sastramas.magnetbali.com').'/api/pengeluaran';
        $tgl = date('Y-m-d');
        $datas  = ModelPengeluaran::where('tanggal', $tgl)->get()->toArray();

        $bearerToken = 'your-bearer-token';
        $headers    =   [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$bearerToken,
            ],
            'http_errors' => false,
        ];

        $payload = [
            'datas' => $datas
        ];
         
        $client = new \GuzzleHttp\Client();
        $request = $client->post($url, [
            'debug' => TRUE,
            'form_params' => $payload,
            'headers' => $headers 
          ]);

        $response = $request->getBody()->getContents();
        \Log::info($response); 

    }
}
