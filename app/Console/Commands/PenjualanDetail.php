<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\PenjualanDetailJob; 

 
use App\Models\PenjualanDetail as ModelPenjualanDetail;  
use GuzzleHttp\Client;
use App\Models\Penjualan as ModelPenjualan; 

class PenjualanDetail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:penjualan_detail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Job Penjualan Detail';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = config('API_URL', 'https://sastramas.magnetbali.com').'/api/penjualan_detail';
        $tgl = date('Y-m-d');
        $datas  = ModelPenjualanDetail::where('tgl', $tgl)->get()->toArray();

        $bearerToken = 'your-bearer-token';
        $headers    =   [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$bearerToken,
            ],
            'http_errors' => false,
        ];

        $payload = [
            'datas' => $datas
        ];
         
        $client = new \GuzzleHttp\Client();
        $request = $client->post($url, [
            'debug' => TRUE,
            'form_params' => $payload,
            'headers' => $headers 
          ]);

        $response = $request->getBody()->getContents(); 
        \Log::info($response); 
    }
}
