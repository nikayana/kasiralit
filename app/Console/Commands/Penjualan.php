<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Jobs\PenjualanJob; 

use GuzzleHttp\Client;
use App\Models\Penjualan as ModelPenjualan; 

class Penjualan extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'job:penjualan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Job Penjualan';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = config('API_URL', 'https://sastramas.magnetbali.com').'/api/penjualan';
        $tgl = date('Y-m-d');
        $datas  = ModelPenjualan::where('tanggal', $tgl)->get()->toArray();

        $bearerToken = 'your-bearer-token';
        $headers    =   [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$bearerToken,
            ],
            'http_errors' => false,
        ];

        $payload = [
            'datas' => $datas
        ];
         
        $client = new \GuzzleHttp\Client();
        $request = $client->post($url, [
            'debug' => TRUE,
            'form_params' => $payload,
            'headers' => $headers 
          ]);

        $response = $request->getBody()->getContents(); 
        \Log::info($response); 
    }
}
