<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use GuzzleHttp\Client;
use App\Models\PenjualanDetail; 

class PenjualanDetailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        $url = config('API_URL', 'https://sastramas.magnetbali.com').'/api/penjualan_detail';
        $tgl = date('Y-m-d');
        $datas  = PenjualanDetail::where('tgl', $tgl)->get()->toArray();

        $bearerToken = 'your-bearer-token';
        $headers    =   [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$bearerToken,
            ],
            'http_errors' => false,
        ];

        $body = [
            'datas' => $datas
        ];
        
        $client = new \GuzzleHttp\Client();
        $request = $client->request('POST',$url, ['form_params' => $body]);
        $response = $request->getBody()->getContents();
        \Log::info($response);
    }
}
