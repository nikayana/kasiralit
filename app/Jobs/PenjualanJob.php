<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

use GuzzleHttp\Client;
use App\Models\Penjualan; 

class PenjualanJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
 
    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    { 
        $url = config('API_URL', 'https://sastramas.magnetbali.com').'/api/penjualan';
        $tgl = date('Y-m-d');
        $datas  = Penjualan::where('tanggal', $tgl)->get()->toArray();

        $bearerToken = 'your-bearer-token';
        $headers    =   [
            'headers' => [
                'Content-Type' => 'application/json',
                'Authorization' => 'Bearer ' .$bearerToken,
            ],
            'http_errors' => false,
        ];

        $payload = [
            'datas' => $datas
        ];
         
        $client = new \GuzzleHttp\Client();
        $request = $client->post($url, [
            'debug' => TRUE,
            'form_params' => $payload,
            'headers' => $headers 
          ]);

        $response = $request->getBody()->getContents();
        \Log::error($response);
    }
}
