<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Charts\Penjualan as GrafikPenjualan;
use App\Models\Penjualan;
use App\Models\Pembelian;
use App\Models\Pendapatan;
use App\Models\Pengeluaran;
use DateTime;
use DateInterval;
use DatePeriod;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    { 
        // After login
        //$user = Auth::user();
        // $config = config('database.connections.mysql');
        // DB::setDefaultConnection('mysql');
        // DB::reconnect('mysql');

        //dd($config);

        $tgl_awal = (!isset($request->start))? date('Y-m-d', strtotime('-7 days')) : date('Y-m-d', strtotime($request->start));
        $tgl_akhir = (!isset($request->end))? date('Y-m-d') : date('Y-m-d', strtotime($request->end));
        
        //widget
        $data = DB::table('tbl_detail_penjualan')->whereBetween('tgl', [ $tgl_awal, $tgl_akhir]); 
        $sum_harga_jual = $data->sum('Sub_total'); 

        $data = DB::table('tbl_detail_penjualan')->whereBetween('tgl', [ $tgl_awal, $tgl_akhir]);
        $sum_harga_pokok = $data->sum('harga_pokok');

        
        //grafik penjualan
        $begin = new DateTime($tgl_awal);
        $end = new DateTime($tgl_akhir);

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);
        $row_tgl = [];
        foreach ($period as $dt) {
            $row_tgl[] = $dt->format("Y-m-d");
        }
        array_push($row_tgl, $tgl_akhir);

        /**PENJUALAN */
        $row_pendapatan = [];
        foreach($row_tgl as $tgl){
            $data = DB::table('tbl_penjualan')->whereBetween('tanggal', [ $tgl, $tgl]); 
            $row_pendapatan[] = $data->count('tanggal');
        }  
        $chart_penjualan = new GrafikPenjualan;
        $chart_penjualan->labels($row_tgl);
        $chart_penjualan->dataset('Faktur Penjualan', 'column', $row_pendapatan); 
        $grafik_penjualan = $chart_penjualan; 

        /**PEMBELIAN */
        $row_pembelian = [];
        $sub_pembelian = 0;
        $sub_pembelian_cash = 0;
        $sub_pembelian_kredit = 0;
        $sub_pembelian_kredit_belum_lunas = 0;
        foreach($row_tgl as $tgl){
            $data = DB::table('tbl_pembelian')->whereBetween('tanggal', [ $tgl, $tgl]); 
            $row_pembelian[] = $data->count('tanggal');
            $sub_pembelian += $data->sum('Sub_total');
            $sub_pembelian_cash += $data->where('Status', 'Cash')->sum('Sub_total');
            $sub_pembelian_kredit += $data->where('Status', 'Kredit')->sum('Sub_total');
            $sub_pembelian_kredit_belum_lunas += $data->where('Status', 'Kredit')->where('Status_Bayar', 'Belum Lunas')->sum('Sub_total');

        }  
        $chart_pebelian = new GrafikPenjualan;
        $chart_pebelian->labels($row_tgl);
        $chart_pebelian->dataset('Faktur Pembelian', 'column', $row_pendapatan); 
        $grafik_pembelian = $chart_pebelian;
        
        $barang = DB::table('tbl_barang')->where('Stok', '<=', 5)->paginate(10);

        /**PENDAPATAN */
        $sum_pendapatan = Pendapatan::whereBetween('tanggal', [$tgl_awal, $tgl_akhir])->sum('nilai');

        /**PENGELUARAN */
        $sum_pengeluaran = Pengeluaran::whereBetween('tanggal', [$tgl_awal, $tgl_akhir])->sum('nilai');
         
        $response = array( 
            'harga_jual' => $sum_harga_jual,
            'harga_pokok' => $sum_harga_pokok, 
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
            'grafik_penjualan' => $grafik_penjualan,
            'grafik_pembelian' => $grafik_pembelian,
            'total_pembelian' => $sub_pembelian,
            'sub_pembelian_cash' => $sub_pembelian_cash,
            'sub_pembelian_kredit' => $sub_pembelian_kredit,
            'sub_pembelian_kredit_belum_lunas' => $sub_pembelian_kredit_belum_lunas,
            'barang' => $barang,
            'pendapatan' => $sum_pendapatan,
            'pengeluaran' => $sum_pengeluaran
        );
        
        return view('home', compact('response'));
    }
}
