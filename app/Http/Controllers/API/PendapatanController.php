<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pendapatan;
use DB;

class PendapatanController extends Controller
{
    
    public function store(Request $request){
        
        if(!empty($request->datas)){
            foreach($request->datas as $item){
                $data = Pendapatan::where('id_pendapatan', $item['id_pendapatan'])->first();
                
                if(empty($data)){
                    $data = new Pendapatan;
                    $data->id_pendapatan = $item['id_pendapatan'];
                    $data->kode_akun = $item['kode_akun'];
                    $data->keterangan = $item['keterangan'];
                    $data->nilai = $item['nilai'];
                    $data->tanggal = $item['tanggal']; 
                    $data->save(); 
                }else{
    
                    $data = Pendapatan::where('id_pendapatan', $item['id_pendapatan'])->update([
                        'id_pendapatan' => $item['id_pendapatan'],
                        'kode_akun' => $item['kode_akun'],
                        'keterangan' => $item['keterangan'],
                        'nilai' => $item['nilai'],
                        'tanggal' => $item['tanggal'], 
                    ]);
    
                } 
    
            } 
            
        }
        
        $response = [
            'success' => true, 
            'pendapatan' => 'sukses'
        ];
        return response()->json($response, 200);
    }
}
