<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\PembelianDetail;
use DB;

class PembelianDetailController extends Controller
{
    
    public function store(Request $request){
        if(!empty($request->datas)){
            foreach($request->datas as $item){
                $data = PembelianDetail::where('No_Faktur_beli', $item['No_Faktur_beli'])
                ->where('kode_brg', $item['kode_brg'])->first(); 
                
                if(empty($data)){
                    $data = new PembelianDetail;
                    $data->No_Faktur_beli = $item['No_Faktur_beli']; 
                    $data->tanggal = $item['tanggal']; 
                    $data->kode_brg = $item['kode_brg']; 
                    $data->nama_brg = $item['nama_brg']; 
                    $data->satuan = $item['satuan']; 
                    $data->qty = $item['qty']; 
                    $data->Hrg_beli = $item['Hrg_beli']; 
                    $data->hrg_pokok = $item['hrg_pokok']; 
                    $data->sub_total = $item['sub_total'];  
                    $data->save(); 
                }else{

                    $data = PembelianDetail::where('No_Faktur_beli', $item['No_Faktur_beli'])
                    ->where('kode_brg', $item['kode_brg'])->update([
                        'No_Faktur_beli' => $item['No_Faktur_beli'],
                        'tanggal' => $item['tanggal'],
                        'kode_brg' => $item['kode_brg'],
                        'nama_brg' => $item['nama_brg'],
                        'satuan' => $item['satuan'],
                        'qty' => $item['qty'],
                        'Hrg_beli' => $item['Hrg_beli'],
                        'hrg_pokok' => $item['hrg_pokok'], 
                        'sub_total' => $item['sub_total'],
                    ]);
                } 
    
            } 
        }
        

        $response = [
            'success' => true, 
            'pembelian_detail' => 'sukses'
        ];
        return response()->json($response, 200);

    
    }
}
