<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Supliyer;
use DB;

class SupliyerController extends Controller
{
    
    public function store(Request $request){
        
        if(!empty($request->datas)){
            foreach($request->datas as $item){
                $data = Supliyer::where('Kode_supplier', $item['Kode_supplier'])->first();
                
                if(empty($data)){
                    $data = new Supliyer;
                    $data->Kode_supplier = $item['Kode_supplier'];
                    $data->nama_supplier = $item['nama_supplier'];
                    $data->Alamat = $item['Alamat'];
                    $data->telp = $item['telp']; 
                    $data->hutang = $item['hutang']; 
                    $data->save(); 
                }else{
    
                    $data = Supliyer::where('Kode_supplier', $item['Kode_supplier'])->update([
                        'nama_supplier' => $item['nama_supplier'],
                        'Alamat' => $item['Alamat'],
                        'telp' => $item['telp'],
                        'hutang' => $item['hutang'], 
                    ]);
    
                }
                
                
    
            } 
            
        }
        
        $response = [
            'success' => true, 
            'suplyer' => 'sukses'
        ];
        return response()->json($response, 200);
    }
}
