<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\PenjualanDetail;
use DB;

class PenjualanDetailController extends Controller
{
    
    public function store(Request $request){
        if(!empty($request->datas)){
            foreach($request->datas as $item){
                $data = PenjualanDetail::where('no_faktur_jual', $item['no_faktur_jual'])
                ->where('kode', $item['kode'])->first(); 
                
                if(empty($data)){
                    $data = new PenjualanDetail;
                    $data->no_faktur_jual = $item['no_faktur_jual']; 
                    $data->tgl = $item['tgl']; 
                    $data->kode = $item['kode']; 
                    $data->uraian = $item['uraian']; 
                    $data->satuan = $item['satuan']; 
                    $data->qty = $item['qty']; 
                    $data->harga_pokok = $item['harga_pokok']; 
                    $data->harga_jual = $item['harga_jual']; 
                    $data->diskon = $item['diskon']; 
                    $data->sub_total = $item['sub_total']; 
                    $data->save(); 
                }else{

                    $data = PenjualanDetail::where('no_faktur_jual', $item['no_faktur_jual'])
                    ->where('kode', $item['kode'])->update([
                        'no_faktur_jual' => $item['no_faktur_jual'],
                        'tgl' => $item['tgl'],
                        'kode' => $item['kode'],
                        'uraian' => $item['uraian'],
                        'satuan' => $item['satuan'],
                        'qty' => $item['qty'],
                        'harga_pokok' => $item['harga_pokok'],
                        'harga_jual' => $item['harga_jual'],
                        'diskon' => $item['diskon'],
                        'sub_total' => $item['sub_total'],
                    ]);
                } 
    
            } 
        }
        

        $response = [
            'success' => true, 
            'penjualan_detail' => 'sukses'
        ];
        return response()->json($response, 200);

    
    }
}
