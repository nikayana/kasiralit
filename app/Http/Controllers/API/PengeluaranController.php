<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pengeluaran;
use DB;

class PengeluaranController extends Controller
{
    
    public function store(Request $request){
        
        if(!empty($request->datas)){
            foreach($request->datas as $item){
                $data = Pengeluaran::where('id_pengeluaran', $item['id_pengeluaran'])->first();
                
                if(empty($data)){
                    $data = new Pengeluaran;
                    $data->id_pengeluaran = $item['id_pengeluaran'];
                    $data->kode_akun = $item['kode_akun'];
                    $data->keterangan = $item['keterangan'];
                    $data->nilai = $item['nilai'];
                    $data->tanggal = $item['tanggal']; 
                    $data->save(); 
                }else{
    
                    $data = Pengeluaran::where('id_pengeluaran', $item['id_pengeluaran'])->update([
                        'id_pengeluaran' => $item['id_pengeluaran'],
                        'kode_akun' => $item['kode_akun'],
                        'keterangan' => $item['keterangan'],
                        'nilai' => $item['nilai'],
                        'tanggal' => $item['tanggal'], 
                    ]);
    
                } 
    
            } 
            
        }
        
        $response = [
            'success' => true, 
            'pengeluaran' => 'sukses'
        ];
        return response()->json($response, 200);
    }
}
