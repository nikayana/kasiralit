<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Penjualan;
use DB;

class PenjualanController extends Controller
{
    
    public function store(Request $request){
        
        if(!empty($request->datas)){
            foreach($request->datas as $item){
                $data = Penjualan::where('no_faktur_jual', $item['no_faktur_jual'])->first();
                
                if(empty($data)){
                    $data = new Penjualan;
                    $data->no_faktur_jual = $item['no_faktur_jual'];
                    $data->tanggal = $item['tanggal'];
                    $data->jam = $item['jam'];
                    $data->status = $item['status'];
                    $data->id_member = $item['id_member'];
                    $data->jmlh_Item = $item['jmlh_Item'];
                    $data->jmlh_qty = $item['jmlh_qty'];
                    $data->nilai_diskon = $item['nilai_diskon'];
                    $data->nilai_dpp = $item['nilai_dpp'];
                    $data->nilai_ppn = $item['nilai_ppn'];
                    $data->total = $item['total'];
                    $data->bayar = $item['bayar'];
                    $data->kembalian = $item['kembalian'];
                    $data->kode_user = $item['kode_user'];
                    $data->ket_pajak = $item['ket_pajak'];
                    $data->save(); 

                     
                }else{
    
                    $data = Penjualan::where('no_faktur_jual', $item['no_faktur_jual'])->update([
                        'no_faktur_jual' => $item['no_faktur_jual'],
                        'tanggal' => $item['tanggal'],
                        'jam' => $item['jam'],
                        'status' => $item['status'],
                        'id_member' => $item['id_member'],
                        'jmlh_Item' => $item['jmlh_Item'],
                        'jmlh_qty' => $item['jmlh_qty'],
                        'nilai_diskon' => $item['nilai_diskon'],
                        'nilai_dpp' => $item['nilai_dpp'],
                        'nilai_ppn' => $item['nilai_ppn'],
                        'total' => $item['total'],
                        'bayar' => $item['bayar'],
                        'kembalian' => $item['kembalian'],
                        'kode_user' => $item['kode_user'],
                        'ket_pajak' => $item['ket_pajak']
                    ]);
                     
    
                }
                
                
    
            } 
            
        }
        
        $response = [
            'success' => true, 
            'penjualan' => 'sukses'
        ];
        return response()->json($response, 200);
    }
}
