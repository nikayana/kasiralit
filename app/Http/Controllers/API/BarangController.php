<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Barang;
use DB;

class BarangController extends Controller
{
    
    public function store(Request $request){
        
        if(!empty($request->datas)){
           
            foreach($request->datas as $item){
                $data = Barang::where('Kode_brg', $item['Kode_brg'])->first();
                 
                if(empty($data)){
                    $data = new Barang;
                    $data->Kode_brg = $item['Kode_brg'];
                    $data->Nama_brg = $item['Nama_brg'];
                    $data->Jenis_brg = $item['Jenis_brg'];
                    $data->Satuan = $item['Satuan'];
                    $data->Tgl_Expired = $item['Tgl_Expired'];
                    $data->Hrg_beli = $item['Hrg_beli'];
                    $data->Hrg_pokok = $item['Hrg_pokok'];
                    $data->Hrg_Grosir = $item['Hrg_Grosir'];
                    $data->Hrg_Eceran = $item['Hrg_Eceran'];
                    $data->Stok = $item['Stok'];
                    $data->stok_masuk = $item['stok_masuk'];
                    $data->stok_keluar = $item['stok_keluar']; 
                    $data->save(); 
                }else{
    
                    $data = Barang::where('Kode_brg', $item['Kode_brg'])->update([
                        'Kode_brg' => $item['Kode_brg'],
                        'Nama_brg' => $item['Nama_brg'],
                        'Jenis_brg' => $item['Jenis_brg'],
                        'Satuan' => $item['Satuan'],
                        'Tgl_Expired' => $item['Tgl_Expired'],
                        'Hrg_beli' => $item['Hrg_beli'],
                        'Hrg_pokok' => $item['Hrg_pokok'],
                        'Hrg_Grosir' => $item['Hrg_Grosir'],
                        'Hrg_Eceran' => $item['Hrg_Eceran'],
                        'Stok' => $item['Stok'],
                        'stok_masuk' => $item['stok_masuk'],
                        'stok_keluar' => $item['stok_keluar'], 
                    ]);
    
                }
                
                
    
            } 
            
        }
        
        $response = [
            'success' => true, 
            'barang' => 'sukses'
        ];
        return response()->json($response, 200);
    }
}
