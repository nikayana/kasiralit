<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Pembelian;
use DB;

class PembelianController extends Controller
{
    
    public function store(Request $request){
        
        if(!empty($request->datas)){
            foreach($request->datas as $item){
                $data = Pembelian::where('No_Faktur_beli', $item['No_Faktur_beli'])->first();
                
                if(empty($data)){
                    $data = new Pembelian;
                    $data->No_Faktur_beli = $item['No_Faktur_beli'];
                    $data->Tanggal = $item['Tanggal'];
                    $data->jam = $item['jam'];
                    $data->kode_supplier = $item['kode_supplier'];
                    $data->nama = $item['nama'];
                    $data->jmlh_Item = $item['jmlh_Item'];
                    $data->jmlh_qty = $item['jmlh_qty'];
                    $data->Sub_total = $item['Sub_total'];
                    $data->tambahan_diskon = $item['tambahan_diskon'];
                    $data->PPn = $item['PPn'];
                    $data->grand_total = $item['grand_total'];
                    $data->kode_user = $item['kode_user'];
                    $data->Status = $item['Status'];
                    $data->Status_Bayar = $item['Status_Bayar']; 
                    $data->save(); 
                }else{
    
                    $data = Pembelian::where('No_Faktur_beli', $item['No_Faktur_beli'])->update([
                        'No_Faktur_beli' => $item['No_Faktur_beli'],
                        'Tanggal' => $item['Tanggal'],
                        'jam' => $item['jam'],
                        'kode_supplier' => $item['kode_supplier'],
                        'nama' => $item['nama'],
                        'jmlh_Item' => $item['jmlh_Item'],
                        'jmlh_qty' => $item['jmlh_qty'],
                        'Sub_total' => $item['Sub_total'],
                        'tambahan_diskon' => $item['tambahan_diskon'],
                        'PPn' => $item['PPn'],
                        'grand_total' => $item['grand_total'],
                        'Status' => $item['Status'],
                        'Status_Bayar' => $item['Status_Bayar'], 
                    ]);
    
                }
                
                
    
            } 
            
        }
        
        $response = [
            'success' => true, 
            'pembelian' => 'sukses'
        ];
        return response()->json($response, 200);
    }
}
