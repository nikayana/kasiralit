<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tgl_awal = (!isset($request->start))? '' : date('Y-m-d', strtotime($request->start));
        $tgl_akhir = (!isset($request->end))? '' : date('Y-m-d', strtotime($request->end));
        

        $barang = DB::table('tbl_barang');
        if($tgl_awal !=''){
            $barang->whereBetween('Tgl_Expired', [ $tgl_awal, $tgl_akhir]);
        }
        if(isset($request->Kode_brg) && !empty($request->Kode_brg)){
            $barang->where('Kode_brg', 'like', '%'.$request->Kode_brg.'%');
        }
        if(isset($request->Nama_brg) && !empty($request->Nama_brg)){
            $barang->where('Nama_brg', 'like', '%'.$request->Nama_brg.'%');
        }
        if(isset($request->Jenis_brg) && !empty($request->Jenis_brg)){
            $barang->where('Jenis_brg', 'like', '%'.$request->Jenis_brg.'%');
        }

        if(isset($request->qty) && $request->qty > 0){
            $barang->where('Stok', '<=', $request->qty);
        }

        $barang = $barang->paginate(10);
         
        $response = array(
            'data' => $barang, 
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
            'request' => $request->all()
        );
         
        
        return view('laporan.barang.index', compact('response'));
         
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
