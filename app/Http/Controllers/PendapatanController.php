<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Pendapatan;
use DB;

class PendapatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $tgl_awal = (!isset($request->start))? date('Y-m-d', strtotime('-30 days')) : date('Y-m-d', strtotime($request->start));
        $tgl_akhir = (!isset($request->end))? date('Y-m-d') : date('Y-m-d', strtotime($request->end));
         
        $data = Pendapatan::whereBetween('tanggal', [$tgl_awal, $tgl_akhir]);
        //filter
        if(isset($request->jenis) && !empty($request->jenis)){
            $data->where('kode_akun', $request->jenis);
        }
        $data_list = $data->orderBy('tanggal', 'desc')->paginate(10);

        $sum_total = $data->sum('nilai');
        $count_total = $data->count();
        
        
        $response = array(
            'data' => $data_list,
            'total' => $sum_total,
            'total_row' => $count_total,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir
        );
        
        
        return view('laporan.pendapatan.index', compact('response'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
