<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Pengeluaran;
use DB;

class PengeluaranController extends Controller
{
    //
    public function index(Request $request)
    {
        $tgl_awal = (!isset($request->start))? date('Y-m-d', strtotime('-30 days')) : date('Y-m-d', strtotime($request->start));
        $tgl_akhir = (!isset($request->end))? date('Y-m-d') : date('Y-m-d', strtotime($request->end));
         
        $data = Pengeluaran::whereBetween('tanggal', [$tgl_awal, $tgl_akhir]);
        //filter
        if(isset($request->jenis) && !empty($request->jenis)){
            $data->where('kode_akun', $request->jenis);
        }
        $data_list = $data->orderBy('tanggal', 'desc')->paginate(10);

        $sum_total = $data->sum('nilai');
        $count_total = $data->count();
        
        
        $response = array(
            'data' => $data_list,
            'total' => $sum_total,
            'total_row' => $count_total,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir
        );
        
        
        return view('laporan.pengeluaran.index', compact('response'));
    }
}
