<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pendapatan extends Model
{
    //
    protected $table = 'tbl_pendapatan';
    public $timestamps = false;
}
