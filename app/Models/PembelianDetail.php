<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PembelianDetail extends Model
{
    //
    protected $table = 'tbl_detail_pembelian';
    public $timestamps = false;
}
