<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pengeluaran extends Model
{
    //
    protected $table = 'tbl_pengeluaran';
    public $timestamps = false;
}
